<x-layout>
    <div class="container mt-5">
        <div class="row">
            @for ($i = 0; $i < 6; $i++)
                <div class="col-md-4">
                    <a class="card mb-4 shadow-sm text-decoration-none" href="#">
                        <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                             xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false"
                             role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title>
                            <rect width="100%" height="100%" fill="#55595c"></rect>
                            <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                        </svg>
                        <div class="card-body text-dark">
                            <h3>This is the title</h3>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.</p>
                            <div class="text-right">
                                <small class="text-muted">By ...</small>
                            </div>
                        </div>
                    </a>
                </div>
            @endfor
        </div>
    </div>
</x-layout>
