<x-cms>
    <div class="container">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Editing news item</h6>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title">
                </div>
                <div class="form-group">
                    <label for="author">Author</label>
                    <select id="author" class="form-control">

                    </select>
                </div>
                <div class="form-group">
                    <label for="intro">Intro</label>
                    <textarea id="intro" cols="30" rows="3" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="html">HTML</label>
                    <textarea id="html" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <button class="btn btn-primary">Save</button>
            </div>
        </div>

    </div>
</x-cms>
