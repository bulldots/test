<x-layout>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <h4 class="mb-3">Contact us</h4>
                <form class="needs-validation" novalidate="">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">First name*</label>
                            <input type="text" class="form-control is-invalid" id="firstName" placeholder="" value="" required="">
                            <div class="invalid-feedback">
                                Valid first name is required.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">Last name*</label>
                            <input type="text" class="form-control" id="lastName" placeholder="" value="" required="">
                            <div class="invalid-feedback">
                                Valid last name is required.
                            </div>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="email">Email*</label>
                        <input type="email" class="form-control" id="email" placeholder="" required="">
                        <div class="invalid-feedback">
                            Please enter a valid email address.
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="message">Message*</label>
                        <textarea id="message" cols="30" rows="10" class="form-control"></textarea>
                        <div class="invalid-feedback">
                            Valid messages is required.
                        </div>
                    </div>

                    <button class="btn btn-primary btn-lg btn-block" type="submit">Send</button>
                </form>
            </div>
        </div>
    </div>
</x-layout>
