<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BaseModel extends Model {
    protected $guarded = [];

    public function __get($key)
    {
        $magicGets = array_filter(get_class_methods($this), function ($method) {
            return Str::startsWith($method, '__get') && $method !== "__get";
        });
        foreach ($magicGets as $magicGet) {
            $res = $this->{$magicGet}($key);
            if ($res !== null) {
                return $res;
            }
        }

        return parent::__get($key);
    }
}
