<?php

namespace App\Http\Traits;

use App\Translation;
use Faker\Factory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

trait Translatable {
    public static function bootTranslatable() {
        static::created(function($item) {
            $faker = Factory::create();
            $title = $faker->title;

            Translation::create([
                "lang"     => App::getLocale(),
                "model"    => get_class($item),
                "model_id" => $item->id,
                "fields"   => [
                    "title" => $title,
                    "slug" => Str::slug($title),
                    "intro" => $faker->paragraph,
                    "content" => $faker->randomHtml()
                ]
            ]);
        });

        static::deleted(function($item) {
            Translation::where([
                "model"    => get_class($item),
                "model_id" => $item->id,
            ])->delete();
        });
    }

    public function __getTranslatable($name) {
        if (!Str::startsWith($name, 'lang_')) {
            return null;
        }

        $fieldName = str_replace("lang_", "", $name);
        $lang = app()->getLocale();

        $parts = explode("_", $fieldName);
        if (count($parts) > 1) {
            $lang = $parts[0];
            $fieldName = $parts[1];
        }

        $translation = $this->translations->where('lang', $lang)->first();

        if (!$translation) {
            return "";
        }

        return Arr::get($translation->fields, $fieldName);
    }

    public function translations() {
        return $this->morphMany(Translation::class, "translatable", "model", "model_id");
    }
}
