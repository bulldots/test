<?php


namespace App;

class Translation extends BaseModel
{
    protected $casts = [
        'fields' => 'array'
    ];

    public function translatable()
    {
        return $this->morphTo();
    }
}
